<body>
<main class="container">
  <div class="row mb-2">
  <?php
  $stringJson = file_get_contents(__DIR__ . '/../json/posts.json');
  $jsonPost = json_decode($stringJson, true);
  $itens = $jsonPost['posts'];
  foreach($itens as $key=>$post){ 
  ?>

<?php 
  if($key == 0){ 
?>
    <div class="col-md-6 equal">
      <div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start">
          <strong class="d-inline-block mb-2 text-success"><?php echo $post["chapeu"] ?></strong>
          <h3 class="mb-0">
            <a class="text-dark" href="<?php echo $post["link"]?>"><?php echo $post["titulo"]?></a>
          </h3>
        <p class="card-text mb-auto"><?php echo $post["linhafina"] ?></p>
        </div>
      </div>
    </div>
 <?php
 }
 ?>

<?php 
  if($key == 1){ 
?>
    <div class="col-md-6  equal">
      <div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start">
          <strong class="d-inline-block mb-2 text-success"><?php echo $post["chapeu"] ?></strong>
          <h3 class="mb-0">
            <a class="text-dark" href="<?php echo $post["link"]?>"><?php echo $post["titulo"]?></a>
          </h3>
        <p class="card-text mb-auto"><?php echo $post["linhafina"] ?></p>
        </div>
      </div>
    </div>
 <?php
 }
 ?>

<?php
  if($key == 2) { 
?>
    <div class="col-md-6">
      <div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start">
          <strong class="d-inline-block mb-2 text-success"><?php echo $post["chapeu"] ?></strong>
            <h3 class="mb-0">
              <a class="text-dark" href="<?php echo $post["link"]?>"><?php echo $post["titulo"]?></a>
            </h3>
            <p class="card-text mb-auto"><?php echo $post["linhafina"] ?></p>
          </div>
        </div>
      </div>
  <?php
      }
  ?>

<?php
  if($key == 3) { 
?>
    <div class="col-md-6">
      <div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start">
          <strong class="d-inline-block mb-2 text-success"><?php echo $post["chapeu"] ?></strong>
            <h3 class="mb-0">
              <a class="text-dark" href="<?php echo $post["link"]?>"><?php echo $post["titulo"]?></a>
            </h3>
            <p class="card-text mb-auto"><?php echo $post["linhafina"] ?></p>
          </div>
        </div>
      </div>
  <?php
      }
  ?>

<?php
  if($key == 4) { 
?>
    <div class="col-md-6">
      <div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start">
          <strong class="d-inline-block mb-2 text-success"><?php echo $post["chapeu"] ?></strong>
            <h3 class="mb-0">
              <a class="text-dark" href="<?php echo $post["link"]?>"><?php echo $post["titulo"]?></a>
            </h3>
            <p class="card-text mb-auto"><?php echo $post["linhafina"] ?></p>
          </div>
        </div>
      </div>
  <?php
      }
  ?>

<?php
  if($key == 5) { 
?>
    <div class="col-md-6">
      <div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start">
          <strong class="d-inline-block mb-2 text-success"><?php echo $post["chapeu"] ?></strong>
            <h3 class="mb-0">
              <a class="text-dark" href="<?php echo $post["link"]?>"><?php echo $post["titulo"]?></a>
            </h3>
            <p class="card-text mb-auto"><?php echo $post["linhafina"] ?></p>
          </div>
        </div>
      </div>
  <?php
      }
  ?>

<?php
  if($key == 6) { 
?>
    <div class="col-md-6">
      <div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start">
          <strong class="d-inline-block mb-2 text-success"><?php echo $post["chapeu"] ?></strong>
            <h3 class="mb-0">
              <a class="text-dark" href="<?php echo $post["link"]?>"><?php echo $post["titulo"]?></a>
            </h3>
            <p class="card-text mb-auto"><?php echo $post["linhafina"] ?></p>
          </div>
        </div>
      </div>
  <?php
      }
    } //fim foreach
  ?>
  
 </div><!-- /.row -->
     
  </main><!-- /.container -->
   
  </body>