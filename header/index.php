<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Teste de Desenvolvimento">
    <meta name="author" content="Thiago Pires">
    <title>Slice TI</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css\blog.css">
</head>

<header class="blog-header py-3 container">
    <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
      <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic">Teste de Desenvolvimento </h1>
             <p class="lead my-3">Teste PHP/JS/HTML para correção de itens com problemas, existe uma imagem que contém exatamente como deve ficar. Procure a imagem na pasta /img/layout/layout.jpg.  Todos os Links devem estar funcionando .</p>
      </div>
    </div>
</header>